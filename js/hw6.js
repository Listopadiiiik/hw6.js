
// Теоретичні питання

// 1. Екранування дозволяє звернутись до певних символів, чисел та літер у строці.
// Також за їх допомогою ми вставляємо у строку лапки.

// 2. Function Expression (виклик через змінну):
// let num = function(){
// }
// далі
// Function Declaration (звичайний виклик):
// function functionName(){
// }
// і також
// Named Function Expression (змішаний тип):
// let a = function funcName(){
// } і ще метод об'єкту (функція як властивість об'єкту)
// const user = {
//     name: "Ben",
//     age: 12,
//     function(){

//     },
// }

// 3. hoisting це коли змінна ще не оголошена, а вже використовується. Але тут логіка така,
// що ця змінна насправді оголошена. Вона просто прописана після команди, в якій ця змінна 
// використовується, і коли виконується код, вона просто "підіймається", хоч і 
// записана знизу.

// Завдання

function createNewUser (){

    newUser = {
        firstName: prompt("Enter your name"),
        lastName: prompt("Enter your last name"),
        birthdayDate: prompt("Enter your birthday date"),
        getLogin(){
            return newUser.firstName.charAt(0).toLowerCase() + newUser.lastName.toLowerCase();
        },
        getPassword(){
            return newUser.firstName.charAt(0).toUpperCase() + newUser.lastName.toLowerCase() + newUser.birthdayDate.slice(6);
        },
        getAge(){
            let superDate = new Date().getFullYear() - newUser.birthdayDate.slice(6);
            if (new Date().getMonth() > newUser.birthdayDate.slice(3,6)){
                return superDate;
            } else {
                return superDate - 1;
            }
        },
     };
     return newUser;
}
let userOne = createNewUser();
console.log(userOne.getLogin());
console.log(userOne.getPassword());
console.log(userOne.getAge());